def get_array_by_user(count: int) -> list:
    array = []
    for i in range(count):
        array.append(int(input("Введите число - ")))
    return array


def get_fectorials(array: list) -> set:
    facts = set()
    for elem in array:
        facts.add(factorial(elem))
    return facts


def factorial(elem):
    fact = 1
    for elem in range(2, elem + 1):
        fact *= elem
    return fact


if __name__ == '__main__':
    array = get_array_by_user(int(input("Введите число элементов - ")))
    print(array)
    print(sum(array))
    print(max(array))
    print(min(array))
    facts = get_fectorials(array)
    print(facts)

